# Catpuccin all css

# What is it
This project provides user css for sites to unify your browsing expirience.\
Better use with dark theme because it's imposible cover every possible  case/class/id/etc. and darkmode prevent killing your eyes at night.

Fork of [solarized-everything-css](https://github.com/alphapapa/solarized-everything-css)

Source of theme [catppuccin-palette](https://github.com/catppuccin/palette/tree/main/scss)

# How to use

## building
1. install sass
2. Edit ./scss/style.scss @import to your theme of coose (themes also inside this dir)
3. build it (if you want or you can use prebuld mocha theme)
``` bash
sh ./build.sh
```
#+end_src

## qutebrowser
```python
 c.content.user_stylesheets = "path/to/style.css"
```


